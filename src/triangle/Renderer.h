#pragma once
#include <array>
#include <memory>
#include <vector>
#include <QQuickItem>
#include <QSGRenderNode>
#include <QOpenGLShaderProgram>

namespace triangle {

/// 2D coordinates for vertex data
struct GlPoint
{
	static int constexpr Dimension = 2;
	GLfloat x, y;
};
GlPoint operator*(GlPoint const &p, QSizeF const &scale);

/// Array to keep a set of vertex coordinates
using VerticesPoints = std::vector<GlPoint>;

using GlColor = std::array<GLfloat, 3>;  ///< Keeps color {R, G, B} in OpenGL format

/// OpenGL renderer of custom QML's triangle item
class Renderer : public QSGRenderNode
{
public:
	Renderer();
	~Renderer();

	void sync(QQuickItem *item);
	void setColor(QColor const &new_color) noexcept;

	// overrides virtual methods from QSGRenderNode
	virtual void render(const RenderState *state) override;
	virtual void releaseResources() override;
	virtual StateFlags changedStates() const override;
	virtual QRectF rect() const override;

private:
	void initialize();

	std::unique_ptr<QOpenGLShaderProgram> program;  ///< Pointer on OpenGL shaders programs
	QSizeF item_size;                               ///< Visual size of QML item
	VerticesPoints const normal_vertices;           ///< Normalized vertices coordinates
	VerticesPoints scaled_vertices;                 ///< Vertices coordinates scaled to size of QML item
	std::vector<GlColor> vertex_colors;             ///< Set of colors for vertices
	int matrix_idx;                                 ///< Keeps index of "matrix" uniform attribute
};

}  // namespace triangle

#pragma once
#include <QQuickItem>

namespace triangle {

/// Wrapper for custom QML triangle renderer
class Item : public QQuickItem
{
	Q_OBJECT
	Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged)

public:
	Item(QQuickItem *parent = nullptr);

	QColor getColor() const noexcept;
	void setColor(QColor const &new_color) noexcept;

	// overrides virtual methods from QQuickItem
	virtual QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;

	/// Helper to register custom QML item in "module_name" module
	static void registerQml(const char *module_name);

signals:
	void colorChanged();

private:
	QColor color;  ///< Keeps last color value applied from QML code
};

}  // namespace triangle

#include <QQuickWindow>
#include <QRunnable>
#include <QDebug>
#include <QSGRenderNode>
#include "Renderer.h"
#include "Item.h"

namespace triangle {

Item::Item(QQuickItem *parent)
    : QQuickItem(parent)
{
	setFlag(ItemHasContents);
}

/*virtual*/ QSGNode *Item::updatePaintNode(QSGNode *node, UpdatePaintNodeData *) /*override*/
{
	QSGRenderNode *n = static_cast<QSGRenderNode *>(node);
	if(!n)
		n = new Renderer;
	auto renderer = static_cast<Renderer *>(n);
	Q_ASSERT(renderer);
	renderer->setColor(color);
	renderer->sync(this);
	return n;
}

QColor Item::getColor() const noexcept
{
	return color;
}

void Item::setColor(QColor const &new_color) noexcept
{
	color = new_color;
	update();
	emit colorChanged();
}

/*static*/ void Item::registerQml(const char *module_name)
{
	qmlRegisterType<Item>(module_name, 1, 0, "Triangle");
}

}  // namespace triangle

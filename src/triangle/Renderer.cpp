#include <algorithm>
#include <QDebug>
#include <QFile>
#include <QOpenGLFunctions>
#include <QQuickWindow>
#include "Renderer.h"

namespace triangle {
namespace {

enum Attribute
{
	Vertex,
	Color
};

char constexpr ResourceShaderPath[] = ":/qml/Triangle/shaders/";

QByteArray readShader(QString const &shader_name)
{
	QFile resource(ResourceShaderPath + shader_name);

	if(resource.open(QIODevice::ReadOnly | QIODevice::Text)) {
		auto shader_prog(resource.readAll());
		qDebug() << "Load" << resource.fileName() << ":\n" << shader_prog;
		return shader_prog;

	} else
		qCritical() << "Error: Can't open file" << resource.fileName();

	return {};
}

VerticesPoints createVertices()
{
	return {{0.5, 0}, {0, 1}, {1, 1}};
}

}  // namespace

Renderer::Renderer()
    : QSGRenderNode()
    , program()
    , item_size()
    , normal_vertices(createVertices())
    , scaled_vertices()
    , vertex_colors(normal_vertices.size(), {1, 1, 1})
    , matrix_idx(-1)
{
	scaled_vertices.reserve(normal_vertices.size());
}

Renderer::~Renderer()
{
	releaseResources();
}

void Renderer::sync(QQuickItem *item)
{
	if(item_size == item->size())
		return;

	item_size = item->size();
	scaled_vertices.clear();
	std::transform(normal_vertices.begin(),
	               normal_vertices.end(),
	               std::back_inserter(scaled_vertices),
	               [this](auto const &p) -> GlPoint { return p * item_size; });
}

void Renderer::initialize()
{
	program = std::make_unique<QOpenGLShaderProgram>();
	program->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, readShader("vshader.glsl"));
	program->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, readShader("fshader.glsl"));
	program->bindAttributeLocation("vertices", Attribute::Vertex);
	program->bindAttributeLocation("colors", Attribute::Color);
	program->link();
	matrix_idx = program->uniformLocation("matrix");
}

/*virtual*/ void Renderer::render(const RenderState *state) /*override*/
{
	if(!program)
		initialize();
	Q_ASSERT(program);
	program->bind();

	Q_ASSERT(matrix_idx != -1);
	program->setUniformValue(matrix_idx, *state->projectionMatrix() * *matrix());

	program->enableAttributeArray(Attribute::Vertex);
	program->enableAttributeArray(Attribute::Color);
	program->setAttributeArray(Attribute::Vertex, GL_FLOAT, scaled_vertices.data(), GlPoint::Dimension);
	program->setAttributeArray(Attribute::Color, GL_FLOAT, vertex_colors.data(), vertex_colors.size());

	QOpenGLFunctions *gl_func = QOpenGLContext::currentContext()->functions();

	gl_func->glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	gl_func->glEnable(GL_BLEND);
	gl_func->glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	// gl_func->glDisable(GL_DEPTH_TEST);
	// gl_func->glEnable(GL_BLEND);
	// gl_func->glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	// Clip support.
	if(state->scissorEnabled()) {
		gl_func->glEnable(GL_SCISSOR_TEST);
		const QRect r = state->scissorRect();  // already bottom-up
		gl_func->glScissor(r.x(), r.y(), r.width(), r.height());
	}
	if(state->stencilEnabled()) {
		gl_func->glEnable(GL_STENCIL_TEST);
		gl_func->glStencilFunc(GL_EQUAL, state->stencilValue(), 0xFF);
		gl_func->glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	}

	gl_func->glDrawArrays(GL_TRIANGLES, 0, scaled_vertices.size());

	program->disableAttributeArray(Attribute::Vertex);
	program->disableAttributeArray(Attribute::Color);
	program->release();
}

/*virtual*/ void Renderer::releaseResources() /*override*/
{
	program.reset();
}

/*virtual*/ QSGRenderNode::StateFlags Renderer::changedStates() const /*override*/
{
	return BlendState | ScissorState | StencilState;
}

/*virtual*/ QRectF Renderer::rect() const /*override*/
{
	return {{0, 0}, item_size};
}

void Renderer::setColor(QColor const &new_color) noexcept
{
	float red = new_color.redF();
	float green = new_color.greenF();
	float blue = new_color.blueF();
	std::rotate(vertex_colors.begin(), vertex_colors.begin() + 1, vertex_colors.end());
	vertex_colors.back() = GlColor{red, green, blue};
}

GlPoint operator*(GlPoint const &p, QSizeF const &scale)
{
	return {p.x * (GLfloat)scale.width(), p.y * (GLfloat)scale.height()};
}

}  // namespace triangle

import QtQuick 2.12
import QtQuick.Controls 2.14

ListView {
	id: root
	signal deleteColor
	signal addColor

	spacing: 5
	clip: true
	ScrollBar.vertical: ScrollBar {}

	model: Config.colors

	delegate: ColorEntry {
		width: parent.width
		color: Config.colors[index]

		ToolTip.visible: down && Config.colors.length > 1
		ToolTip.text: "Hold long press to delete"

		onPressAndHold: {
			if (Config.colors.length > 1) {
				root.currentIndex = index
				root.deleteColor()
			}
		}
	}

	footer: ItemDelegate {
		width: parent.width
		text: "Add new color"
		font.bold: true
		onClicked: root.addColor()
	}

	function update() {
		root.model = Config.colors.length
	}
}

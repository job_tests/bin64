import QtQuick 2.14
import QtQuick.Controls 2.14

Item {
	id: root
	property int decimals: 2
	readonly property alias realValue: spin.realValue

	implicitHeight: spin.height
	implicitWidth: spin.width

	SpinBox {
		id: spin
		readonly property real scale: Math.pow(10, decimals)
		property real realValue: Config.speed
		property bool init: false

		editable: true
		from: 1
		to: 100 * scale
		stepSize: scale / 10 / 2
		value: 1

		Component.onCompleted: {
			value = realValue * scale
			init = true
		}

		validator: DoubleValidator {
			bottom: Math.min(spin.from, spin.to)
			top: Math.max(spin.from, spin.to)
		}

		textFromValue: function (value, locale) {
			return Number(value / scale).toLocaleString(locale, 'f', decimals)
		}

		valueFromText: function (text, locale) {
			return Number.fromLocaleString(locale, text) * scale
		}

		onValueChanged: {
			if (!init)
				return
			realValue = value / scale
			Config.speed = realValue
		}
	}
}

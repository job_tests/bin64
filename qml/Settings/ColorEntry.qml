import QtQuick 2.14
import QtQuick.Controls 2.14

ItemDelegate {
	id: root
	property color color: "transparent"
	anchors.margins: 5
	implicitHeight: lbl.height + anchors.topMargin + anchors.bottomMargin
	implicitWidth: lbl.width + anchors.leftMargin + anchors.rightMargin

	background: Rectangle {
		anchors.fill: parent
		color: root.down ? Qt.darker(root.color, 2) : root.color
		border {
			color: root.down ? root.color : Qt.darker(root.color, 1.5)
			width: 2
		}
		radius: height / 4
	}

	Label {
		id: lbl
		anchors.centerIn: parent
		text: root.color
		font {

			bold: true
			pointSize: 25
			family: "Courier"
			capitalization: Font.AllUppercase
		}
		color: isDarkColor(root.color) ? Config.light_text : Config.dark_text
	}

	function isDarkColor(background) {
		var temp = Qt.darker(background, 1)
		var a = 1 - (0.299 * temp.r + 0.587 * temp.g + 0.114 * temp.b)
		return temp.a > 0 && a >= 0.3
	}
}

pragma Singleton

import QtQuick 2.0

QtObject {
	property real speed: 1
	property int color_timeout: 500
	property bool clockwise: false
	property color current_color: "black"
	property var colors: ["yellow", "blue", "cyan", "magenta"]
	readonly property color light_text: "white"
	readonly property color dark_text: "black"

	property int _color_idx: 0

	function nextColor() {
		if (_color_idx >= colors.length - 1)
			_color_idx = 0
		else
			++_color_idx
		current_color = colors[_color_idx]
	}

	function deleteColor(index) {
		var value = colors[index]
		colors = colors.filter(function (elem) {
			return elem !== value
		})
		nextColor()
	}

	Component.onCompleted: nextColor()
}

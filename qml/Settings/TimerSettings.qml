import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

RowLayout {
	id: root
	signal applyValue
	property alias value: spin.value

	spacing: 5
	Label {
		text: "Timeout to change color"
	}
	SpinBox {
		id: spin
		editable: true
		from: 1
		to: 5000
		stepSize: 100
		onValueChanged: root.applyValue()
	}
	Label {
		text: "ms."
	}
}

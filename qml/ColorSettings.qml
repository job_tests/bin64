import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import QtQuick.Dialogs 1.2
import Settings 1.0

Page {
	title: "Color selection"
	padding: 10

	ColorDialog {
		id: color_dialog
		title: "Select new color"
		onAccepted: {
			Config.colors.push(this.color.toString())
			colors_list.update()
		}
	}

	MessageDialog {
		id: confirm_dialog
		title: "Confirmation"
		text: "Delete this collor"
		icon: StandardIcon.Question
		standardButtons: StandardButton.Yes | StandardButton.No
		onYes: {
			Config.deleteColor(colors_list.currentIndex)
			colors_list.update()
		}
	}

	ColumnLayout {
		anchors.fill: parent
		TimerSettings {
			value: Config.color_timeout
			onApplyValue: Config.color_timeout = value
		}

		ColorList {
			id: colors_list
			Layout.fillWidth: true
			Layout.fillHeight: true

			onAddColor: color_dialog.open()
			onDeleteColor: confirm_dialog.open()
		}
	}
}

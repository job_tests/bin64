attribute highp vec4 vertices;
attribute lowp vec4 colors;
uniform highp mat4 matrix;

varying lowp vec4 v_color;

void main()
{
	v_color = colors;
	gl_Position = matrix * vertices;
}

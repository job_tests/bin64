import QtQuick 2.0
import Settings 1.0
import GlItems 1.0
import Controls 1.0

Item {
	id: root
	ColorTimer {
		id: timer
	}

	Triangle {
		id: tri
		anchors.fill: root
		color: Config.current_color
		transform: Rotation {
			origin {
				x: tri.width / 2
				y: tri.height / 2
			}
			axis {
				x: 0
				y: 1
				z: 0
			}
			NumberAnimation on angle {
				id: anim
				from: Config.clockwise ? 180 : 0
				to: Config.clockwise ? 0 : 180
				duration: 1000 / (Config.speed > 0 ? Config.speed : 1)
				loops: Animation.Infinite
				easing.type: Easing.InOutQuad
			}
		}


		onVisibleChanged: {
			if (visible) {
				timer.start()
				anim.start()
			} else {
				timer.stop()
				anim.stop()
			}
		}
	}
}

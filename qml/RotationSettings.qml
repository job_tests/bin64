import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import Settings 1.0

Page {
	title: qsTr("Rotation settings")
	padding: 10

	ColumnLayout {
		RowLayout {
			Label {
				text: "Rotation speed"
			}
			RotationSpeed {}
			Label {
				text: "times/sec."
			}
		}

		RowLayout {
			Label {
				text: "Direction"
			}
			Switch {
				id: _sw
				checked: Config.clockwise
				onCheckedChanged: Config.clockwise = checked
			}
			Label {
				text: _sw.checked ? "Clockwise" : "Counterclockwise"
			}
		}
	}
}

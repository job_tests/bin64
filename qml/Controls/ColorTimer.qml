import QtQuick 2.14
import Settings 1.0

Timer {
	interval: Config.color_timeout
	running: true
	repeat: true
	onTriggered: Config.nextColor()
}
